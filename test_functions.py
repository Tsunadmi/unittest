import functions
import unittest

class TestFunc(unittest.TestCase):
    def test_add(self):
        self.assertEqual(functions.add(5,5),10)
        self.assertEqual(functions.add(-1,1),0)
        self.assertEqual(functions.add(450,550),1000)
        self.assertEqual(functions.add(-25,-5),-30)
    def test_mean(self):
        self.assertEqual(functions.mean(5,5),5)
        self.assertEqual(functions.mean(-1,1),0)
        self.assertEqual(functions.mean(450,550),500)
        self.assertEqual(functions.mean(-25,-5),-15)
    def test_sub(self):
        self.assertEqual(functions.sub(12,5),7)
        self.assertEqual(functions.sub(-1,4),-5)
        self.assertEqual(functions.sub(450,550),-100)
        self.assertEqual(functions.sub(-13,-13),0)
    def test_mult(self):
        self.assertEqual(functions.mult(3,5),15)
        self.assertEqual(functions.mult(-1,4),-4)
        self.assertEqual(functions.mult(100,10),1000)
        self.assertEqual(functions.mult(12,12),144)
    def test_cube(self):
        self.assertEqual(functions.cube(3),27)
        self.assertEqual(functions.cube(1),1)
        self.assertEqual(functions.cube(2),8)
        self.assertEqual(functions.cube(5),125)
    def test_even_symbols(self):
        self.assertEqual(functions.even_symbols('qwer'),'qe')
        self.assertEqual(functions.even_symbols('123456789'),'13579')
        self.assertEqual(functions.even_symbols('dima'),'dm')
        self.assertEqual(functions.even_symbols('dima+masa'),'dm+aa')
    def test_polyndrom(self):
        self.assertEqual(functions.polyndrom('qwer'),'rewq')
        self.assertEqual(functions.polyndrom('123456789'),'987654321')
        self.assertEqual(functions.polyndrom('dima'),'amid')
        self.assertEqual(functions.polyndrom('dima+masa'),'asam+amid')
    def test_wedding(self):
        self.assertEqual(functions.wedding(100),'ресторан')
        self.assertEqual(functions.wedding(51),'ресторан')
        self.assertEqual(functions.wedding(25),'кафе')
        self.assertEqual(functions.wedding(1),'дом')
    def test_mult_neg_2(self):
        self.assertEqual(functions.mult_neg_2([1,1,1]),[-2,-2,-2])
        self.assertEqual(functions.mult_neg_2([10,20,30]),[-20,-40,-60])
        self.assertEqual(functions.mult_neg_2([0,0,0]),[0,0,0])
        self.assertEqual(functions.mult_neg_2([-1,-2,-3]),[2,4,6])
    def test_sum_elements_of_numbers(self):
        self.assertEqual(functions.sum_elements_of_numbers(123),6)
        self.assertEqual(functions.sum_elements_of_numbers(55555),25)
        self.assertEqual(functions.sum_elements_of_numbers(1000000000),1)
        self.assertEqual(functions.sum_elements_of_numbers(123456789),45)
    def test_mult_elements_of_numbers(self):
        self.assertEqual(functions.mult_elements_of_numbers(123),6)
        self.assertEqual(functions.mult_elements_of_numbers(555),125)
        self.assertEqual(functions.mult_elements_of_numbers(1000000000),0)
        self.assertEqual(functions.mult_elements_of_numbers(22222),32)
    def test_dev(self):
        self.assertEqual(functions.dev(2,2),1)
        self.assertEqual(functions.dev(2,0),'ZeroDivisionError')
        self.assertEqual(functions.dev(100,10),10)
        self.assertEqual(functions.dev(458,0),'ZeroDivisionError')
    def test_get_index_row_max_sum_numbers(self):
        self.assertEqual(functions.get_index_row_max_sum_numbers([[1,2,3],[1,5,2],[4,8,3]]),2)
        self.assertEqual(functions.get_index_row_max_sum_numbers([[100,220,3],[-1,56,150],[0,0,0]]),0)
        self.assertEqual(functions.get_index_row_max_sum_numbers([[1,1,1],[1,1,2],[1,1,1]]),1)
        self.assertEqual(functions.get_index_row_max_sum_numbers([[5000,-499,3000],[2200,2200,2200],[2500,2500,2500]]),0)



if __name__ =='__main__':
    unittest.main()