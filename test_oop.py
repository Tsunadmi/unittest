from OOP import Pracovnik
import unittest

class TestOpp(unittest.TestCase):
    def test_email(self):
        person1 = Pracovnik('Ivan', 'Pupkin', 'Tester')
        person2 = Pracovnik('Urij','Stojanov','Showman')
        person3 = Pracovnik('Ilija','Prusikin','Singer')

        self.assertEqual(person1.email, 'Ivan.Pupkin@gmail.com')
        self.assertEqual(person2.email, 'Urij.Stojanov@gmail.com')
        self.assertEqual(person3.email, 'Ilija.Prusikin@gmail.com')

        person1.first = 'Dmitry'

        self.assertEqual(person1.email, 'Dmitry.Pupkin@gmail.com')

    def test_form_list(self):
        person1 = Pracovnik('Ivan', 'Pupkin', 'Tester')
        person2 = Pracovnik('Urij','Stojanov','Showman')
        person3 = Pracovnik('Ilija','Prusikin','Singer')

        self.assertEqual(person1.form_list, 'Ivan Pupkin is Tester')
        self.assertEqual(person2.form_list, 'Urij Stojanov is Showman')
        self.assertEqual(person3.form_list, 'Ilija Prusikin is Singer')


if __name__ =='__main__':
    unittest.main()