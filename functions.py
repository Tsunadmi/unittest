def add(a,b):
    return a+b

def mean(a, b):
    return (a+b)/2

def sub (a,b):
    return a-b

def mult (a,b):
    return a*b

def cube(a):
   return pow(a,3)

def even_symbols(string):
    return string[::2]

def polyndrom(string):
    return string[::-1]

def wedding(people_amount):
    if people_amount > 50:
        return 'ресторан'
    elif 50 >= people_amount >= 20:
        return 'кафе'
    elif people_amount < 20:
        return 'дом'

def mult_neg_2(some_list):
    return [x * -2 for x in some_list]

def sum_elements_of_numbers(nmb):
    a = str(nmb)
    sum = 0
    for i in a:
        sum += int(i)
    return sum

def mult_elements_of_numbers(nmb):
    a = str(nmb)
    mult = 1
    for i in a:
        mult *=int(i)
    return mult

def dev(a,b):
    try:
        return (a / b)
    except ZeroDivisionError:
        return 'ZeroDivisionError'

def get_index_row_max_sum_numbers(array):
    r = [sum(i) for i in array]
    return r.index(max(r))
