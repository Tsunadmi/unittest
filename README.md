# Unittest

Simple test app with unittest biblioteca for mathematical function and OOP.

## Installation

You need install python 3.9:

```
pip install python
```

## Important import
```
import unittest

```

## Getting started

For mathematical test app strat:

```
python test_functions.py
```

For oop test app strat:

```
python test_oop.py
```

## Technologies used

- Python
- JetBrains
- math
- oop


## Description

Simple test program just for fun :)


## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)
