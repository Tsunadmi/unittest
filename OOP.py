class Pracovnik:
    def __init__(self, first, last, position):
        self.first = first
        self.last = last
        self.position = position

    @property
    def email(self):
        return f'{self.first}.{self.last}@gmail.com'

    @property
    def form_list(self):
        return f'{self.first} {self.last} is {self.position}'